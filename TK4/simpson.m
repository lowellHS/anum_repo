function out = simpson(f, a, b, n)
    if nargin == 3
        n = 100;
    end
    out = (f(a) + 4*f((a + b)/2) + f(b))/6;
    h = (b - a)/n;
    for i=1:(n-1)
        out = out + f(a+i*h);
    end
    out = out * h;
end