function out = trapezoid(f, a, b, n)
    if nargin == 3
        n = 1800;
    end
    out = (f(a)+f(b))/2;
    h = (b - a)/n;
    for i=1:(n-1)
        out = out + f(a+i*h);
    end
    out = out * h;
end