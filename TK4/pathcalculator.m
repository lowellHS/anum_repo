%Path Calculator
%Program ini menerima n titik dari pengguna
%dan menginterpolasi titik-titik ini
%dengan menggunakan kurva smooth
%kemudian mengambarkannya dan menghitung
%panjangnya.
function pathcalculator(n)
    plot([-1 1],[0,0],'k',[0 0],[-1 1],'k'); hold on
    
    t = 1:n;
    xdata = zeros(1,n);
    ydata = zeros(1,n);
    for i=1:n
        [x,y] = ginput(1)
        plot(x, y, 'bo');
        xdata(i) = x;
        ydata(i) = y;    
    end
    
    %proses xdata dan ydata untuk menggambarkan path
    %dan menghitung panjang path
    spline3_plot(xdata', ydata', t', 1);

end