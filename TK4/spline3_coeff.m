function [a, b, c, d] = spline3_coeff(x, y)
    n = size(x)(1);
    
    delta_y = y(2:n) - y(1:n-1);

    h = x(2:n) - x(1:n-1);

    miu = [];
    lambda = [];
    gamma = [];
    for i=1:n-2
        tmp = h(i)/(h(i) + h(i+1));
        tmp2 = 6*(delta_y(i+1)/h(i+1)-delta_y(i)/h(i));
        tmp2 = tmp2/(h(i)+h(i+1));
        miu = [miu; tmp];
        lambda = [lambda; 1-tmp];
        gamma = [gamma; tmp2];
    end

    A = 2*eye(n-2,n-2);

    for k=2:n-2
        A(k,k-1) = miu(k);
    end

    for k=1:n-3
        A(k,k+1) = lambda(k);
    end

    tao = A \ gamma;
    tao = [0;tao;0];


    d = y;
    b = tao/2;
    c = [];
    a = [];
    for i=1:n-1
        tmpa = (tao(i+1)-tao(i))/(6*h(i));
        tmpc = delta_y(i)/h(i) - h(i)*((2*tao(i)+tao(i+1))/6);
        c = [c; tmpc];
        a = [a; tmpa];
    end
end