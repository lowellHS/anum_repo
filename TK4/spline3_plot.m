function out = spline3_plot(x1, y1, t1, visualize)
    [a1,b1,c1,d1] = spline3_coeff(t1, x1);
    [a2,b2,c2,d2] = spline3_coeff(t1, y1);

    tt = min(t1):0.01:max(t1);
    tt = tt';

    [x2 conditions x_ders] = spline3_eval(a1,b1,c1,d1,t1,tt);
    [y2 conditions y_ders] = spline3_eval(a2,b2,c2,d2,t1,tt);
    
    zz = 2;
    path_length = 0;
    n = size(tt)(1);
    for i=1:n-1
        curr = tt(i);
        if curr > t1(zz)
            zz = zz + 1;
        end
        k = zz - 1;
        f = @(u) sqrt((x_ders(k){:}(u))^2 + (y_ders(k){:}(u))^2);
        path_length = path_length + simpson(f, tt(i), tt(i+1), 10);
    end
    out = path_length;
    
    if visualize
        plot(x1, y1, 'o'); hold on;
        plot(x2, y2);
        text(-0.9, 0.9, sprintf("Total length = %.16f", path_length))
    end
end