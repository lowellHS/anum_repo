x = (-3:3)';
n = max(size(x));
y = -x.^2 + 9;
t = (1:n)';

actual_length = 0.5 * (6 * sqrt(37) + asinh(6));
approx_length = spline3_plot(x, y, t, 0);
error = approx_length - actual_length

x = (-3:0.1:3)';
n = max(size(x));
y = sqrt(9 - x.^2);
t = (1:n)';

actual_length = pi * 3;
approx_length = spline3_plot(x, y, t, 0);
error = approx_length - actual_length
