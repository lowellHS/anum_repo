function [y_test, conditions, S_der] = spline3_eval(a, b, c, d, x, x_test)
    n = size(x)(1);
    
    conditions = {};
    S = {};
    S_der = {};
    for i=1:n-1
        f = @(u) u <= x(i+1) & u >= x(i);
        s = @(u) a(i)*(u - x(i))^3 + b(i)*(u - x(i))^2 + c(i)*(u - x(i)) + d(i);
        s_der = @(u) 3*a(i)*(u - x(i))^2 + 2*b(i)*(u - x(i)) + c(i);
        conditions{end+1} = f;
        S{end+1} = s;
        S_der{end+1} = s_der;
    end
    
    zz = 2;
    y_test = [];
    for i=1:size(x_test)(1)
        curr = x_test(i);
        if curr > x(zz)
            zz = zz + 1;
        end
        y_test = [y_test; S(zz-1){:}(curr)];
    end
end