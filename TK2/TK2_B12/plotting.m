x = -pi:0.01:pi;

[m, n] = size(x)
y = zeros(m, n);
for i=1:n
    y(i) = f(x(i));
end

plot(x,y);hold on;
plot([-pi/4, pi/4], [0, 0],'o');
