function count = f_p2_sol(p2)
    delta = 0.01;
    tol = 0.5 * 10 ^ -8;

    count = 0;
    for i=-pi:delta:pi
        a = i;
        b = i+delta;
        if sign(f_p2(a, p2)) != sign(f_p2(b, p2))
            while (b-a) > tol
                m = (a + b)/2;
                if sign(f_p2(a, p2)) == sign(f_p2(b, p2))
                    a = m;
                else
                    b = m;
                end
            end
            count = count + 1;
        end
    end
end
