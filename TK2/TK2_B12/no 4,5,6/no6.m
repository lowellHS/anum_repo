clear all;

f6 = @(x) f6(x);

tol = 0.5 * 10 ^ -8;

##theta1 = secant(f6, 1, 1.3, tol)
##theta2 = secant(f6, 1.9, 2, tol)

theta1 = bisection(f6, 1, 1.3, tol)
theta2 = bisection(f6, 1.9, 2, tol)

gambar6(theta1); figure
gambar6(theta2);

# nilai p2 = 4.5
# nilai p2 didapatkan dengan mencoba - coba angka, lalu plot grafiknya,
# jika grafiknya memotong sumbu tepat di 2 titik yang berbeda. 
# maka angka tersebut yang diambil. 