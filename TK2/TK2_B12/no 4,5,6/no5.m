clear all;

f5 = @(x) f5(x);

tol = 0.5 * 10 ^ -8;

theta1 = bisection(f5, -0.8, -0.6, tol)
theta2 = bisection(f5, -0.4, -0.2, tol)
theta3 = bisection(f5, -0.1, 0.1, tol)
theta4 = bisection(f5, 0.4, 0.6, tol)
theta5 = bisection(f5, 0.8, 1, tol)
theta6 = bisection(f5, 2.4, 2.6, tol)

gambar5(theta1); figure
gambar5(theta2); figure
gambar5(theta3); figure
gambar5(theta4); figure
gambar5(theta5); figure
gambar5(theta6);
