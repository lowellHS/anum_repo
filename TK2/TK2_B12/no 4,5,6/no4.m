f4 = @(x) f4(x);

tol = 0.5 * 10 ^ -8;

theta1 = bisection(f4, -1, -0.5, tol)
theta2 = bisection(f4, -0.5, 0, tol)
theta3 = bisection(f4, 1, 1.5,  tol)
theta4 = bisection(f4, 2, 2.5, tol)

##disp('=================================')

##theta1 = secant(f4, -1, -0.5, tol)
##theta2 = secant(f4, -0.5, 0, tol)
##theta3 = secant(f4, 1, 1.5,  tol)
##theta4 = secant(f4, 2, 2.5, tol)

gambar4(theta1); figure
gambar4(theta2); figure
gambar4(theta3); figure
gambar4(theta4);
