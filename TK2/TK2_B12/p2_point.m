function res = p2_point(a, b, l, r)
    tol = 0.5 * 10 ^ -8;
    if l != r
        while (b-a) > tol
            m = (a + b)/2;
            if (f_p2_sol(a) == f_p2_sol(b))
                a = m;
            else
                b = m;
            end
        end
        res = a;
    end
end
