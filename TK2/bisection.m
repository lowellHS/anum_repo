function x = bisection(f, a, b, tol)
  while ((b - a) > tol) 
    
    m = (a + b) /2;
    fm = f(m);
    
    if fm == 0
      break
    endif
    
    if sign(f(a)) == sign(f(b))
      a = m;
    else
      b = m;
    endif
    
  endwhile
  
  x = (a + b) / 2;
  
endfunction
