function [p1, p2, p3] = gambar5(theta)
  
  x1 = 5
  y1 = 0

  x2 = 0
  y2 = 6

  l2 = 3 * sqrt(2);
  l3 = 3;
  gama = pi / 4;
  
  [out, x, y] = f5(theta)

  u1 = x
  u2 = x + l3 * cos(theta)
  u3 = x + l2 * cos(theta + gama)

  v1 = y
  v2 = y + l3 * sin(theta)
  v3 = y + l2 * sin(theta + gama)

  p1 = sqrt((0 - u1) ^ 2 + (0 - v1) ^ 2)
  p2 = sqrt((x1 - u2) ^ 2 + (y1 - v2) ^ 2)
  p3 = sqrt((x2 - u3) ^ 2 + (y2 - v3) ^ 2)

  plot([u1, u2, u3, u1], [v1, v2, v3, v1], 'r'); hold on
  plot([0, x1, x2], [0, y1, y2], 'bo');
  plot([0, u1], [0, v1]);
  plot([x1, u2], [y1, v2]);
  plot([x2, u3], [y2, v3]);
  title(sprintf('\\theta = %.4f', theta))
  xlabel(sprintf('p1 = %.4f, p2 = %.4f, p3 = %.4f', p1,p2,p3))
  hold off;
 
  
endfunction