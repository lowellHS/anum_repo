function [x] = secant(f, x0, x1, tol)
    f_x0 = f(x0);
    f_x1 = f(x1);

    while abs(f_x1) > tol
        try
            denominator = (f_x1 - f_x0)/(x1 - x0);
            x = x1 - (f_x1)/denominator;
        catch
            break
        end
        x0 = x1;
        x1 = x;
        f_x0 = f_x1;
        f_x1 = f(x1);
   
    end

    x = x1;
    
end