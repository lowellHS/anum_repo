f = @(x) f_asal(x);

tol = 0.5 * 10 ^ -10;

theta1 = bisection(f, 1.4884, 1.4984, tol);
theta2 = bisection(f, 1.5684, 1.5784, tol);

x1 = 5;
y1 = 0;

x2 = 0;
y2 = 6;

l2 = 3 * sqrt(2);
l3 = 3;
gama = pi / 4;

[out, x, y] = f_asal(theta1);

u1 = x;
u2 = x + l3 * cos(theta1);
u3 = x + l2 * cos(theta1 + gama);

v1 = y;
v2 = y + l3 * sin(theta1);
v3 = y + l2 * sin(theta1 + gama);

p2 = sqrt((x1 - u2) ^ 2 + (y1 - v2) ^ 2)
 