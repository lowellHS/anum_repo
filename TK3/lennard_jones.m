pkg load symbolic;

syms x1 x2 y1 y2 z1 z2;

r = sqrt((x1-x2)^2 + (y1-y2)^2 + (z1-z2)^2);

var_num = [1 5];
x = sym('x', var_num);
y = sym('y', var_num);
z = sym('z', var_num);

U = 0;

old = {x1 x2 y1 y2 z1 z2};
for i=1:5
    for j=1:5
        if j > i
            new = {x(i) x(j) y(i) y(j) z(i) z(j)};
            rij = subs(r, old, new);
            U = U + (1/(rij)^12 - 2/(rij)^6);
        end
    end
end

vars = [x y z];
sym_grad = gradient(U, vars);
sym_hessian = jacobian(sym_grad, vars);

f = function_handle(U);
grad_f = function_handle(sym_grad);
hessian_f = function_handle(sym_hessian);

save variables/dump.m f grad_f hessian_f U sym_grad sym_hessian vars;
