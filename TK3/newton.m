function [output, x_history] = newton(f, vars, TOL, xk, grad_f, hessian_f, max_iter)
    n = length(vars);
    if nargin == 2
        TOL = 1e-8;
        xk = zeros(n, 1);
    elseif nargin == 3
        xk = zeros(n, 1);
    end
    x_history = [];
    
    if nargin < 5
        sym_grad_f = gradient(f, vars);
        sym_hessian_f = jacobian(sym_grad_f, vars);
        grad_f = function_handle(sym_grad_f);
        hessian_f = function_handle(sym_hessian_f);
        max_iter = 100;
    end
    
    % inputs{:} for convert array so that match arguments
    % like [1,2,4] to (1,2,4) so we can call f(1,2,4)
    inputs = num2cell(xk);
    while norm(grad_f(inputs{:})) >= TOL && max_iter > 0
        x_history = [x_history xk];
        H = hessian_f(inputs{:});
        b = grad_f(inputs{:});
        s = H \ b;
        xk = xk - s;
        inputs = num2cell(xk);
        max_iter = max_iter - 1;
    end 
    
    output = xk; 
    x_history = [x_history xk];
end