function out = get_3d(f, range)
    figure;
    colormap jet;
    [X, Y] = meshgrid(range, range);
    Z = f(X, Y);
    surf(X,Y,Z);
    saveas(gcf, 'plot_3d.png');
end