function [output, x_history] = steepest_descent(f, vars, TOL, xk)
    n = length(vars);
    if nargin == 2
        TOL = 1e-8;
        xk = zeros(n, 1);
    elseif nargin == 3
        xk = zeros(n, 1);
    end
    x_history = [];
    
    sym_grad_f = gradient(f, vars);
    f = function_handle(f);
    grad_f = function_handle(sym_grad_f);
    
    % inputs{:} for convert array so that match arguments
    % like [1,2,4] to (1,2,4) so we can call f(1,2,4)
	max_iter = 25;
    inputs = num2cell(xk);
    while norm(grad_f(inputs{:})) >= TOL && max_iter > 0
        x_history = [x_history xk];
        pk = -grad_f(inputs{:});
        alpha = armijo(f, grad_f, pk, xk);
        
        xk = xk + alpha .* pk;
        inputs = num2cell(xk);
        max_iter = max_iter - 1;
    end 
    
    output = xk; 
    x_history = [x_history xk];
end