function alpha = armijo(f, grad_f, pk, xk)
    alpha = 1;
    l = 0;
    a = f(num2cell(xk + alpha .* pk){:});
    b = f(num2cell(xk){:}) + 0.1 * alpha * grad_f(num2cell(xk){:})' * pk;
    while (a > b)
        alpha = 0.5 * alpha;
        l = l + 1;
        a = f(num2cell(xk + alpha .* pk){:});
        b = f(num2cell(xk){:}) + 0.1 * alpha * grad_f(num2cell(xk){:})' * pk;
    end
end