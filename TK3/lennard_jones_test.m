warning('off');
load variables/dump.m;

coor = reshape([
                     0                   0                   0
                     0                   0               0.001
                 0.005                -0.9                0.41
                  -0.1                 0.3               -0.051
                   0.1                 0.2               0.002
], 1, []);

[a b] = newton(U, vars, 1e-4, coor', grad_f, hessian_f, 100);

% coor = rand(15,1);
% [a b] = newton(U, vars, 1e-4, coor, grad_f, hessian_f, 100);

coors = {};
for j=1:5
    points = [];
    for i=1:size(b)(2)
        tmp = b(:,i);
        points = [points [tmp(j);tmp(j+5);tmp(j+10)]];
    end
    coors{end+1} = points;
end

coor
func = f(num2cell(a){:})

% colors = [
    % 1.0 .00 .20;
    % .80 .20 .00;
    % .60 .40 1.0;
    % .40 .60 .80;
    % .20 .80 .60;
    % .00 1.0 .40;
% ];

% sec_der = hessian_f(num2cell(a){:});
% func = f(num2cell(a){:})
% eig(round(sec_der))
% chol(sec_der)
% chol(-sec_der)

% for i=1:5
    % atom = coors(i){:};
    % n = length(atom);
    
    % figure;
    % scatter3(atom(1,:), atom(2,:), atom(3,:), 'MarkerEdgeColor', 'k', 'MarkerFaceColor', colors(i,:));
    % hold on;
    % scatter3(atom(1,1), atom(2,1), atom(3,1), 'MarkerEdgeColor', 'k', 'MarkerFaceColor', [0 0 0]);
    % scatter3(atom(1,n), atom(2,n), atom(3,n), 'MarkerEdgeColor', 'k', 'MarkerFaceColor', [1 1 1]);
    % plot3(atom(1,:), atom(2,:), atom(3,:));
    % xlabel('x');
    % ylabel('y');
    % zlabel('z');
    % saveas(gcf, sprintf('atom_%d.png', i));
% end

% xs = a(1:5,1); xs = [xs; xs(1)];
% ys = a(6:10,1); ys = [ys; ys(1)];
% zs = a(11:15,1); zs = [zs; zs(1)];

% figure;
% scatter3(a(1:5,1), a(6:10,1), a(11:15,1), 'MarkerEdgeColor', 'k', 'MarkerFaceColor', [0 0.75 0.75]);
% hold on;
% plot3(xs, ys, zs);
% xlabel('x');
% ylabel('y');
% zlabel('z');
% saveas(gcf, 'atom_final.png');