function [f, vars] = ackley_d(d)	
	x = sym('x', [1 d]);
	
	sum1 = 0;
	sum2 = 0;
	for i=1:d
		sum1 = sum1 + x(i).^2;
		sum2 = sum2 + cos(2*pi*x(i));
	end
	sum1 = -0.2 * sqrt(sum1 / d);
	sum2 = sum2 / d;
	
	f = -20 * exp(sum1) - exp(sum2) + 20 + exp(1);
    vars = x;
end