function [output, x_history] = quasi_newton(f, vars, TOL, xk)
    n = length(vars);
    if nargin == 2
        TOL = 1e-8;
        xk = zeros(n, 1);
    elseif nargin == 3
        xk = zeros(n, 1);
    end
    x_history = [];
    
    sym_grad_f = gradient(f, vars);
    f = function_handle(f);
    grad_f = function_handle(sym_grad_f);
    B = eye(n);
    
    % inputs{:} for convert array so that match arguments
    % like [1,2,4] to (1,2,4) so we can call f(1,2,4)
	max_iter = 25;
    inputs = num2cell(xk);
    while norm(grad_f(inputs{:})) >= TOL && max_iter > 0
        x_history = [x_history xk];
        pk = -B * grad_f(inputs{:});
        alpha = armijo(f, grad_f, pk, xk);
        
        next_xk = xk + alpha * pk;
        delta = next_xk - xk;
        gamma = grad_f(num2cell(next_xk){:}) - grad_f(num2cell(xk){:});
        
        temp = delta - B * gamma;
        numerator = temp * temp';
        denominator = temp' * gamma;
        
        if denominator > eps(0)
            B = B + numerator/denominator;
        else
            B = B;
        end
        
        xk = next_xk;
        inputs = num2cell(xk);
        max_iter = max_iter - 1;
    end 
    
    output = xk; 
    x_history = [x_history xk];
end