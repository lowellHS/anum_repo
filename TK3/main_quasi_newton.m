pkg load symbolic;
warning('off', 'all');

tol = 10 ^ -3;

# ====================================================
# d = 2
[f, vars] = ackley_d(2);
fungsi = function_handle(f);

% # plot
% [x,y]= meshgrid(-5:.2:5,-5:.2:5);
% in = [x(:), y(:)];
% out = fungsi(x,y);
% z = reshape(out, size(x));
% surf(x, y, z)

% # input 1 (konvergen ke minimum global)
% x = zeros(2, 1);
% tic();
% [output x_history] = quasi_newton(f, vars, tol, x);
% elapsed_time = toc()
% output
% disp("------")


% # input 2 (konvergen ke minimum local)
% x2 = 3 * ones(2, 1);
% tic();
% [output x_history] = quasi_newton(f, vars, tol, x2);
% elapsed_time = toc()
% output
% disp("------")



% # ====================================================
% # d = 8 
% [f, vars] = ackley_d(8);
% fungsi = function_handle(f);  

% # input 1 (konvergen ke minimum global)
% x = zeros(8, 1);
% tic();
% [output x_history] = quasi_newton(f, vars, tol, x);
% elapsed_time = toc() 
% output
% disp("------")


% # input 2 (konvergen ke minimum local)
% x2 = 3 * ones(8, 1);
% tic();
% [output x_history] = quasi_newton(f, vars, tol, x2);
% elapsed_time = toc() 
% output
% disp("------")




% # ====================================================
% # d = 32
[f, vars] = ackley_d(32);
fungsi = function_handle(f);  

# input 1 (konvergen ke minimum global)
x = zeros(32, 1);
tic();
[output x_history] = quasi_newton(f, vars, tol, x);
elapsed_time = toc()
output
disp("------")


# input 2 (konvergen ke minimum local)
x2 = 3 * ones(32, 1);
tic();
[output x_history] = quasi_newton(f, vars, tol, x2);
elapsed_time = toc()   
output
disp("------")

