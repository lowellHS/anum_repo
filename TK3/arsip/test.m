f = @(x,y) (x.^2 + y - 11).^2 + (y.^2 + x - 7).^2;
grad_f = @(x,y) [
    4.*x.^3 + 4.*x.*y - 42.*x + 2.*y.^2 - 14;
    4.*y.^3 + 4.*x.*y - 26.*y + 2.*x.^2 - 22;
];
hessian_f = @(x,y) [
    12.*x.^2+4.*y-42,   4.*x+4.*y;
    4.*y+4.*x,          12.*y.^2+4.*x-26;
];
% range1 = 0.9:0.1:3.1;
% get_3d(f, range2);

TOL = 1e-8;
% range = 0.9:0.1:3.1;

% [points1, history1] = newton(f, grad_f, hessian_f, TOL, [2.5;1.5]);
% [points2, history2] = steepest_descent(f, grad_f, TOL, [1;0.9]);
% [points3, history3] = quasi_newton(f, grad_f, TOL, [1;1]);

% get_contour(f, range, history1, history2, history3);

% mean_time = 0;
% for j=1:10
    % tic;
    % for i=1:10
        % newton(f, grad_f, hessian_f, TOL, [1;1]);
        % steepest_descent(f, grad_f, TOL, [1;1]);
        % quasi_newton(f, grad_f, TOL, [1;1]);
    % end
    % mean_time = mean_time + toc;
% end
% mean_time = mean_time / 10;
% disp(mean_time);