function out = get_contour(f, range, histories, histories2, histories3)
    figure;
    [X, Y] = meshgrid(range, range);
    Z = f(X, Y);
    
    if nargin > 2
        contour(X,Y,Z,50); hold on;
        
        coors = size(histories)(2);
        x1 = histories(1,1:coors);
        y1 = histories(2,1:coors);
        
        coors = size(histories2)(2);
        x2 = histories2(1,1:coors);
        y2 = histories2(2,1:coors);
        
        coors = size(histories3)(2);
        x3 = histories3(1,1:coors);
        y3 = histories3(2,1:coors);
        
        plot(x1,y1, '-o');
        plot(x2,y2, '-o');
        plot(x3,y3, '-o');
        
        legend('', 'Newton', 'Steepest Descent', 'Quasi-Newton SR1', 'Location', 'SouthEast')
    elseif
        contour(X,Y,Z,50);
    end
    
    saveas(gcf, 'plot_contour.png');
end