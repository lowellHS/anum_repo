pkg load symbolic;
warning('off', 'all');

d = 2;

[sym_f, vars] = ackley_d(d);

range = [-0.0001:0.00001:0.0001];

sym_grad = gradient(sym_f, vars);
grad_f = function_handle(sym_grad);

norms = [];
x = ones(d,1);
for i=range
    inputs = num2cell(x*i);
    norms = [norms norm(grad_f(inputs{:}))];
end

plot(range, norms, 'r');