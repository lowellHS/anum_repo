function y = forward_elimination_trid(L, b, l)
% Forward Elimination
% finding y such that Ly = b
% L is a lower triangular matrix
[n n] = size(L);
y = zeros(n,1);
for i=1:n
    y(i) = b(i);
    for j=find(l == i)
        y(i) = y(i) - L(i,j)*y(j);
    end
end