function x = spl_lu_partial_unoptimize_trid(A, b)
% A is tridiagonal matrix
% find x that satisfy Ax = b, using tridiagonal factorization partial pivoting
[n, n] = size(A);
[L, U, P] = factorization_lu_partial_trid(A);

x = backward_substitution(U, forward_elimination(L, P*b));