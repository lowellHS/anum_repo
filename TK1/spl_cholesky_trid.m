function x = spl_cholesky_trid(A, b)
% A is tridiagonal positive definit matrix
% find x that satisfy Ax = b, using tridiagonal cholesky factorization
[n, n] = size(A);
[G] = factorization_cholesky_trid(A);

L = G;
U = G';

y = zeros(size(b));
y(1,1) = b(1,1)/L(1,1);
for k=2:n
    y(k,1) = (b(k,1) - L(k,k-1)*y(k-1,1))/L(k,k);
end

u = zeros(size(b));
u(n,1) = y(n,1)/U(n,n);
for k=n-1:-1:1
    u(k,1) = (y(k,1) - U(k,k+1)*u(k+1,1))/U(k,k);
end

x=u;