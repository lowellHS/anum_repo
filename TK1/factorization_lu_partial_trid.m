function [L, U, P, l] = factorization_lu_partial_trid(A)
%Input matriks A
%output matriks L & U dan vektor pivot P
[n n] = size(A);
L = eye(n);
p = 1:n;
l = 2:n;

for i=1:n-1
    c = i:min(i+3,n);
    if A(i+1,i) > A(i,i)
        tempp = p(i);
        p(i) = p(i+1);
        p(i+1) = tempp;
        
        tempA = A(i,:);
        A(i,:) = A(i+1,:);
        A(i+1,:) = tempA;
        
        tmpL=L(i,1:i-1);
        L(i,1:i-1)=L(i+1,1:i-1);
        L(i+1,1:i-1)=tmpL;
        
        l(l == i) = i+1;
    end
    const = A(i+1,i)/A(i,i);
    L(i+1,i) = const;
    A(i+1,:) = A(i+1,:) - L(i+1,i)*A(i,:);
end

U = A;

P = zeros(n,n);
for i=1:n
    P(i,p(i)) = 1;
end