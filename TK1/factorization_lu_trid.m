function [L, U] = factorization_lu_trid(A)
% tridiagonal lu factorization
[n n] = size(A);
L = eye(n);

for i=2:n
    L(i,i-1) = A(i,i-1)/A(i-1,i-1);
    A(i,i) = A(i,i) - L(i,i-1)*A(i-1,i);
end

U = A;