function x = spl_lu_trid(A, b)
% A is tridiagonal matrix
% find x that satisfy Ax = b, using tridiagonal factorization
[n, n] = size(A);
[L, U] = factorization_lu_trid(A);

y = zeros(size(b));
y(1,1) = b(1,1);
for k=2:n
    y(k,1) = b(k,1) - L(k,k-1)*y(k-1,1);
end

u = zeros(size(b));
u(n,1) = y(n,1)/U(n,n);
for k=n-1:-1:1
    u(k,1) = (y(k,1) - U(k,k+1)*u(k+1,1))/U(k,k);
end

x=u;