function [G] = factorization_cholesky_trid(A)
% tridiagonal cholesky factorization
[n n] = size(A);
G = zeros(n);

G(1,1) = sqrt(A(1,1));
for i=2:n
    G(i,i-1) = A(i,i-1)/G(i-1,i-1);
    G(i,i) = sqrt(A(i,i) - G(i,i-1)^2);
end

