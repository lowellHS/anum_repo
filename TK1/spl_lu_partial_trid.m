function x = spl_lu_partial_trid(A, b)
% A is tridiagonal matrix
% find x that satisfy Ax = b, using tridiagonal factorization partial pivoting
[n, n] = size(A);
[L, U, P, l] = factorization_lu_partial_trid(A);

x = backward_substitution_trid(U, forward_elimination_trid(L, P*b, l));