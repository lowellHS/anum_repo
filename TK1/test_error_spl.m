A1 = [
		0.35185   0.29554   0.00000;
		0.17453   0.08829   0.12708;
		0.00000   0.77004   0.02357;
 ];
 
A2 = [
		0.86969   0.53083   0.00000   0.00000   0.00000;
		0.62094   0.07353   0.88210   0.00000   0.00000;
		0.00000   0.20100   0.03663   0.04927   0.00000;
		0.00000   0.00000   0.43746   0.81217   0.79586;
		0.00000   0.00000   0.00000   0.46118   0.61458;
];

A3 = [
		0.00081   0.14876   0.00000   0.00000   0.00000   0.00000   0.00000;
		0.70572   0.87726   0.64797   0.00000   0.00000   0.00000   0.00000;
		0.00000   0.73965   0.19786   0.71989   0.00000   0.00000   0.00000;
		0.00000   0.00000   0.26481   0.06496   0.76397   0.00000   0.00000;
		0.00000   0.00000   0.00000   0.21555   0.06561   0.67104   0.00000;
		0.00000   0.00000   0.00000   0.00000   0.43233   0.16884   0.92755;
		0.00000   0.00000   0.00000   0.00000   0.00000   0.65828   0.11377;
];

%============================================================================================================%

b1 = [
		0.5314
		0.2299
		0.5058
];

b2 = [
		1.2343
		1.1946
		0.2499
		1.5038
		0.8062
];

b3 = [
		0.0978
		0.9628
		1.0270
		0.1155
		0.3401
		0.1033
		0.1875
];

%============================================================================================================%

max_size = 50;
spl_system_error = zeros(max_size,1);
spl_lu_trid_error = zeros(max_size,1);
spl_lu_partial_trid_error = zeros(max_size,1);
spl_cholesky_trid_error = zeros(max_size,1);

%============================================================================================================%
% -----------------------------------------------------------------------------------------------------------
%                               Test Backward Error for Tridiagonal Matrix
% -----------------------------------------------------------------------------------------------------------
%
spl_system_error(1,1) = norm(A1*(A1 \ b1) - b1);
spl_system_error(2,1) = norm(A2*(A2 \ b2) - b2);
spl_system_error(3,1) = norm(A3*(A3 \ b3) - b3);
for i=4:max_size
    [A,x,b] = generate_trid_spl(i);
    spl_system_error(i,1) = norm(A*(A \ b) - b);
end

spl_lu_trid_error(1,1) = norm(A1*spl_lu_trid(A1, b1) - b1);
spl_lu_trid_error(2,1) = norm(A2*spl_lu_trid(A2, b2) - b2);
spl_lu_trid_error(3,1) = norm(A3*spl_lu_trid(A3, b3) - b3);
for i=4:max_size
    [A,x,b] = generate_trid_spl(i);
    spl_lu_trid_error(i,1) = norm(A*spl_lu_trid(A, b) - b);
end

spl_lu_partial_trid_error(1,1) = norm(A1*spl_lu_partial_trid(A1, b1) - b1);
spl_lu_partial_trid_error(2,1) = norm(A2*spl_lu_partial_trid(A2, b2) - b2);
spl_lu_partial_trid_error(3,1) = norm(A3*spl_lu_partial_trid(A3, b3) - b3);
for i=4:max_size
    [A,x,b] = generate_trid_spl(i);
    spl_lu_partial_trid_error(i,1) = norm(A*spl_lu_partial_trid(A, b) - b);
end

disp('Backward Error for Tridiagonal Matrix:')
disp(sprintf('System                                             : %0.4e', norm(spl_system_error)));
disp(sprintf('Tridiagonal LU Factorization                       : %0.4e', norm(spl_lu_trid_error)));
disp(sprintf('Tridiagonal LU Factorization with Partial Pivoting : %0.4e', norm(spl_lu_partial_trid_error)));

%============================================================================================================%
% -----------------------------------------------------------------------------------------------------------
%                       Test Backward Error for Positive Definit Tridiagonal Matrix
% -----------------------------------------------------------------------------------------------------------
%
for i=1:max_size
    [A,x,b] = generate_postdef_spl(i+3);
    spl_system_error(i,1) = norm(A*(A \ b) - b);
end

for i=1:max_size
    [A,x,b] = generate_postdef_spl(i+3);
    spl_lu_trid_error(i,1) = norm(A*spl_lu_trid(A, b) - b);
end

for i=1:max_size
    [A,x,b] = generate_postdef_spl(i+3);
    spl_lu_partial_trid_error(i,1) = norm(A*spl_lu_partial_trid(A, b) - b);
end

for i=1:max_size
    [A,x,b] = generate_postdef_spl(i+3);
    spl_cholesky_trid_error(i,1) = norm(A*spl_cholesky_trid(A, b) - b);
end
disp('Backward Error for Positive Definit Tridiagonal Matrix:')
disp(sprintf('System                                             : %0.4e', norm(spl_system_error)));
disp(sprintf('Tridiagonal LU Factorization                       : %0.4e', norm(spl_lu_trid_error)));
disp(sprintf('Tridiagonal LU Factorization with Partial Pivoting : %0.4e', norm(spl_lu_partial_trid_error)));
disp(sprintf('Tridiagonal Cholesky Factorization                 : %0.4e', norm(spl_cholesky_trid_error)));

%============================================================================================================%