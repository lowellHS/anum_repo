function [A,x,b] = generate_trid_spl(n)
% random tridiagonal matrix with size n * n
A = rand(n,n);
for i=1:n-2
    for j=i+2:n
        A(i,j) = 0;
        A(j,i) = 0;
    end
end

x = rand(n,1);
b = A * x;