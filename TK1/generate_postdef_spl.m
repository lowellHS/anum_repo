function [A,x,b] = generate_postdef_spl(n)

A = [
        2 1 0
        1 2 1
        0 1 2
];

curr_size = 3;
while curr_size < n
    A = cat(1,A,zeros(1,size(A,1)));
    A = cat(2,A,zeros(size(A,1),1));
    curr_size = size(A,1);
    A(curr_size,curr_size) = 2;
    A(curr_size,curr_size - 1) = 1;
    A(curr_size - 1,curr_size) = 1;
end

x = ones(curr_size,1);
b = A * x;