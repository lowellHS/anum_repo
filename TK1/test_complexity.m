
max_size = 510;

sizes = 10:100:max_size;
complexity1 = zeros(size(sizes,2),1);
complexity2 = zeros(size(sizes,2),1);
complexity3 = zeros(size(sizes,2),1);

counter = 1;
for n=sizes
    [A,x,b] = generate_postdef_spl(n);
    tic;
    spl_lu_trid(A, b);
    complexity1(counter,1) = toc;
    tic;
    spl_lu_partial_trid(A, b);
    complexity2(counter,1) = toc;
    tic;
    spl_cholesky_trid(A, b);
    complexity3(counter,1) = toc;
    disp(counter);
    counter += 1;
end

plot(sizes, complexity1, sizes, complexity2, sizes, complexity3);
xlabel('n (Ukuran matriks n x n)');
ylabel('Waktu (detik)');
legend({
            'LU Tridiagonal factorization', 
            'LU factorization partial pivoting',
            'Cholesky factorization',
       }, 'Location','northeastoutside');