
max_size = 100;

sizes = 1:10:max_size;
complexity1 = zeros(size(sizes,2),1);
complexity2 = zeros(size(sizes,2),1);

counter = 1;
for n=sizes
    [A,x,b] = generate_postdef_spl(n);
    tic;
    spl_lu_partial_trid(A, b);
    complexity1(counter,1) = toc;
    tic;
    spl_lu_partial_unoptimize_trid(A, b);
    complexity2(counter,1) = toc;
    tic;
    disp(counter);
    counter += 1;
end

plot(sizes, complexity1, sizes, complexity2);
xlabel('n (Ukuran matriks n x n)');
ylabel('Waktu (detik)');
legend({
            'Optimal',
            'Tidak optimal',
       }, 'Location','northeastoutside');