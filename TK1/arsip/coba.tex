\documentclass[a4paper, 12pt]{article}
\usepackage{amsmath}
\usepackage{multicol}
\usepackage[margin=0.5in]{geometry}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{hyperref}

\lstset{
	language=Matlab,
	aboveskip=3mm,
	belowskip=3mm,
	showstringspaces=false,
	columns=flexible,
	basicstyle={\small\ttfamily},
	numbers=none,
	breaklines=true,
	breakatwhitespace=true,
	tabsize=3
}
\setlength\columnsep{30pt}
\begin{document}
	
	\title{Tugas 1 Analisis Numerik : SPL Tridiagonal}
	\author{Harlan H., Lowell H. S., Melkisedek B. S.}
	\date{March 2, 2019}
	\maketitle
	\pagenumbering{gobble}
	
	\begin{multicols}{2}
		\section{Introduction}
		\textit{Tridiagonal Matrix} merupakan matriks spesial dan termasuk dalam \textit{banded matrix}. Sebuah matrix $A$ dengan ukuran $n$ dikatakan \textit{banded matrix}, jika terdapat \textit{integer} positif $p$ dimana $p$ $<$$<$ $n$ sehingga $A_{ij}$ = 0 jika $|i - j| > p$.
		Jika $p = 1$ maka \textit{banded matrix} disebut \textit{tridiagonal matrix}, contohnya sebagai berikut \\
		\[
		A_{4\times4} = \begin{bmatrix}
		a_{1,1} & a_{1,2} & 0 & 0\\
		a_{2,1} & a_{2,2} & a_{2,3} & 0\\
		0 & a_{3,2} & a_{3,3} & a_{3,4}\\
		0 & 0 & a_{4,3} & a_{4,4}\\
		\end{bmatrix}
		\]
		\[
		A_{n \times n} = \begin{bmatrix}
		a_{1,1} & a_{1,2} &  0 & \dots & 0 \\
		a_{2,1} & a_{2,2} &  a_{2,3} & \ddots & \vdots \\
		0 & a_{3,2} & \ddots & \ddots & 0 \\
		\vdots & \ddots & \ddots  & a_{n-1,n-1} & a_{n-1,n} \\
		0 & \dots & 0 & a_{n,n-1}  & a_{n,n} \\
		\end{bmatrix}
		\]
		Terlihat bahwa banyak entri dari matriks yang bernilai $0$, sehingga jika suatu sistem persamaan linear tridiagonal $Ax = b$ maka terdapat algoritma optimal untuk mendapatkan solusi. Pada tugas ini akan dibandingkan beberapa algoritma yang terdiri dari faktorisasi $LU$ tridiagonal, faktorisasi $LU$ tridiagonal dengan \textit{partial pivoting} dan faktorisasi \textit{Cholesky} dengan A matriks tridiagonal dan definit positif.
		\\
		\\
		Sebuah SPL dapat ditemukan solusinya ($A$ memiliki invers) menggunakan algoritma faktorisasi $LU$ dengan \textit{partial pivoting}. Hal ini juga berlaku untuk SPL tridiagonal, namun kita dihadapkan pada masalah kompleksitas. Saat melakukan \textit{pivoting}, setiap entri pada sebuah kolom (di bawah diagonal) harus dilihat, padahal cukup melihat 2 entri saja. Selain itu saat melakukan \textit{forward elimination} banyak entri yang tidak perlu dilakukan perhitungan atau bernilai $0$. Pada saat melakukan \textit{backward substitution} juga hanya dibutuhkan beberapa entri saja untuk suatu baris tidak perlu semua entri. Perlu dilakukan optimisasi pada algoritma menggunakan \textit{partial pivoting}.
		\\
		\\
		Oleh karena itu, kami ingin membuktikan apakah ketiga algoritma yang sudah disebutkan dapat menyelesaikan SPL tridiagonal dengan optimal. 
		
		\section{Content}
		\subsection{Faktorisasi $LU$ tridiagonal}
		(Catatan: $A$ = matriks tridiagonal, $L$ = matriks \textit{lower triangular}, $U$ = matriks \textit{upper triangular}, $n$ = ukuran matriks ($n\times n$), $G$ = matriks \textit{lower triangular} hasil faktorisasi \textit{Cholesky}, kompleksitas pada tugas ini fokus pada kompleksitas flops)\\\\
		Implementasi algoritma faktorisasi $LU$ tridiagonal\textsuperscript{1} menggunakan \textit{Octave} sebagai berikut,
		\begin{lstlisting}
	[n n] = size(A);
	L = eye(n);
	
	for i=2:n
		L(i,i-1) = A(i,i-1)/A(i-1,i-1);
		A(i,i) = A(i,i) - L(i,i-1)*A(i-1,i);
	end
	
	U = A;
		\end{lstlisting}
		Untuk faktorisasi LU di atas kompleksitasnya adalah $\sum_{i=2}^{n}(3) = \sum_{i=1}^{n-1}(3) = 3(n-1) = 3n - 3$.\\
		Setelah didapatkan matriks $L$ dan $U$ maka $Ax = b$ menjadi $LUx = b$. Misalkan $Ux = y$ maka $Ly = b$, sehingga untuk mencari $y$ adalah sebagai berikut,
		\begin{lstlisting}
	y = zeros(size(b));
	y(1,1) = b(1,1);
	for k=2:n
		y(k,1) = b(k,1) - L(k,k-1)*y(k-1,1);
	end
		\end{lstlisting}
		Untuk mendapatkan $y$ kompleksitasnya adalah $\sum_{k=2}^{n}(2) = \sum_{k=1}^{n-1}(2) = 2(n-1) = 2n - 2$.\\\\
		Kemudian dicari $x$ sesuai persamaan $Ux = y$ dengan $U$ dan $y$ yang sudah diketahui,
		\begin{lstlisting}
	u = zeros(size(b));
	u(n,1) = y(n,1)/U(n,n);
	for k=n-1:-1:1
		u(k,1) = (y(k,1) - U(k,k+1)*u(k+1,1))/U(k,k);
	end
		\end{lstlisting}
		Untuk mendapatkan $x$ dengan cara di atas kompleksitasnya adalah $1 + \sum_{k=1}^{n-1}(3) = 1 + 3(n-1) = 3n - 2$.\\
		Total kompleksitas untuk mendapatkan solusi $x$ dari SPL tridiagonal $Ax=b$ menggunakan algoritma ini adalah $(3n - 3) + (2n - 2) + (3n - 2) = 8n - 7 \approx 8n$.
		\subsection{Faktorisasi $LU$ tridiagonal dengan \textit{partial pivoting}}
		Implementasi algoritma faktorisasi $LU$ tridiagonal dengan \textit{partial pivoting} 
		\begin{lstlisting}
	[n n] = size(A);
	L = eye(n);
	p = 1:n;
	l = 2:n;
	
	for i=1:n-1									% (1)
		c = i:min(i+3,n);
		if A(i+1,i) > A(i,i)					% (2)
			tempp = p(i);
			p(i) = p(i+1);
			p(i+1) = tempp;
			
			tempA = A(i,:);
			A(i,:) = A(i+1,:);
			A(i+1,:) = tempA;
			
			tmpL=L(i,1:i-1);
			L(i,1:i-1)=L(i+1,1:i-1);
			L(i+1,1:i-1)=tmpL;
			
			l(l == i) = i+1;
			end
		const = A(i+1,i)/A(i,i);		  % (3)
		L(i+1,i) = const;
		A(i+1,:) = A(i+1,:) - L(i+1,i)*A(i,:);
	end
	
	U = A;
	
	P = zeros(n,n);							% (4)
	for i=1:n
		P(i,p(i)) = 1;
	end
		\end{lstlisting}
		Terlihat bahwa perbedaan dari algoritma sebelumnya terletak pada kode bagian (2) dan (4). Pada bagian (2) dilakukan proses \textit{swapping}. Pencarian pivot cukup membandingkan 2 entri yaitu $A_{i,i}$ dengan $A_{i+1,i}$ dan cari yang lebih besar. Jika entri bawah lebih besar maka lakukan \textit{swapping}. Pada bagian (4) merupakan proses pembentukan matriks permutasi dari vektor permutasi $p$.\\\\
		Matriks $L$ yang dihasilkan dari faktorisasi $LU$ tridiagonal memiliki bentuk yang unik. Setiap kolom pada matriks $L$ hanya terdiri dari 2 entri yang dapat bernilai tidak nol yaitu entri $L_{i,i}$ bernilai 1 dan $L_{k,i}$ ($1\le i \le n$, $i<k \le n$), sehingga vektor $l$ berguna untuk mencatat baris ke-$k$ tersebut. Misalkan $l = [4,4,5,5,5]$ berarti hanya terdapat 2 baris yaitu 4 dan 5 dimana yang dapat bernilai tak nol pada baris ke-4 adalah kolom ke-1 dan ke-2, sedangkan pada baris ke-5 adalah kolom ke-3, ke-4 dan ke-5. Hal ini dilakukan untuk optimisasi proses \textit{forward elimination}.
		Algoritma ini mirip dengan algoritma sebelumnya, terlihat pada bagian (3) yang melakukan operasi terhadap matriks $A$ dan mencatat konstanta pada matriks $L$. Kompleksitas faktorisasi menggunakan \textit{partial pivoting} adalah $3n - 3$, karena tidak ada tambahan operasi \textit{floating point} dari algoritma faktorisasi sebelumnya.\\\\
		Perlu dicari $LU = b$ dengan tahapan (1) $Ly = b$ dan (2) $Ux = y$. Mencari $y$ pada tahap (1) menggunakan \textit{forward elimination},
		\begin{lstlisting}
	[n n] = size(L);
	y = zeros(n,1);
	for i=1:n
		y(i) = b(i);
		for j=find(l == i)				% (1)
			y(i) = y(i) - L(i,j)*y(j);
		end
	end
		\end{lstlisting}
		Pada \textit{forward elimination} di atas terdapat proses optimisasi pada bagian (1) dimana hanya mencari pada $L_{i,1}$ sampai $L_{i,i-1}$ pada baris ke-$i$ tertentu. Hal ini dibantu oleh vektor $l$ pada faktorisasi yang telah dilakukan. Kompleksitasnya adalah $2n - 2$, asumsi matriks $L$ dimana entri tak 0 semuanya berada pada baris ke-$n$, sehingga operasi yang dilakukan pada baris ke-1 sampai ke-($n-1)$ hanya \textit{assignment} dan tidak terhitung flops, tetapi pada baris ke-$n$ dilakukan 1 kali \textit{assignment} untuk $L_{n,n}$ dan $2$ flops pada $L_{n,1}$ sampai $L_{n,n-1}$ sehingga menjadi $2(n - 1) = 2n - 2$.\\\\
		Mencari $x$ pada tahap (2) menggunakan \textit{backward substitution},
		\begin{lstlisting}
	[n n] = size(U);
	x = zeros(n,1);
	for i=n:-1:1
		x(i) = b(i);
		for j=i+1:min(i+2,n)				% (1)
			x(i) = x(i) - U(i,j)*x(j);
		end
		x(i) = x(i) / U(i,i);
	end
		\end{lstlisting}
		Iterasi paling luar dilakukan sebanyak $n$ kali, tetapi pada $i = n$ hanya terjadi operasi \textit{assignment}, sehingga yang diperhitungkan hanya $n - 1$ iterasi. Pada bagian (1) hanya dilakukan pencarian pada dua kolom setelah kolom ke-$i$ karena sifat matriks tridiagonal. Maksimal dilakukan dua iterasi pada bagian (1). Sehingga kompleksitas adalah $\sum_{i=1}^{n-1}(\sum_{j=1}^{2}(2) + 1) = \sum_{i=1}^{n-1}(5) = 5n - 5$.\\
		Total kompleksitas untuk mendapatkan solusi $x$ dari SPL tridiagonal $Ax=b$ menggunakan \textit{partial pivoting} adalah $(3n - 3) + (2n - 2) + (5n - 5) = 10n - 10 \approx 10n$.
		
		\subsection{Faktorisasi \textit{Cholesky} tridiagonal}
		Implementasi algoritma faktorisasi \textit{Cholesky} tridiagonal dengan faktorisasi $LU$ tanpa \textit{pivoting} sangat mirip hanya terdapat perbedaan yaitu matriks $A$ harus definit positif dan terdapat operasi \textbf{sqrt} (\textit{square root}). (Asumsi bahwa operasi \textit{sqrt} membutuhkan 1 flop).
		\begin{lstlisting}
	[n n] = size(A);
	G = zeros(n);
	
	G(1,1) = sqrt(A(1,1));
	for i=2:n
		G(i,i-1) = A(i,i-1)/G(i-1,i-1);
		G(i,i) = sqrt(A(i,i) - G(i,i-1)^2);
	end
		\end{lstlisting}
		Kompleksitasnya adalah $1 + \sum_{i=2}^{n}(4) = 1 + \sum_{i=1}^{n-1}(4) = 1 + 4(n - 1) = 1 + 4n - 4 = 4n - 3$.\\\\\\\\
		Tahap mendapatkan $Ly = b$ dimana $L = G$
		\begin{lstlisting}
	L = G;
	
	y = zeros(size(b));
	y(1,1) = b(1,1)/L(1,1);
	for k=2:n
		y(k,1) = (b(k,1) - L(k,k-1)*y(k-1,1))/L(k,k);
	end
		\end{lstlisting}
		Kompleksitasnya adalah $1 + \sum_{k=1}^{n-1}(3) = 3n - 2$.\\\\
		Tahap mendapatkan $Ux = y$ dimana $U = G'$ (transpose dari G)
		\begin{lstlisting}
	U = G';
	
	u = zeros(size(b));
	u(n,1) = y(n,1)/U(n,n);
	for k=n-1:-1:1
		u(k,1) = (y(k,1) - U(k,k+1)*u(k+1,1))/U(k,k);
	end
		\end{lstlisting}
		Kompleksitasnya adalah $1 + \sum_{k=1}^{n-1}(3) = 3n - 2$.\\
		Total kompleksitas untuk mendapatkan solusi $x$ dari SPL tridiagonal $Ax=b$ menggunakan algoritma ini adalah $(4n - 3) + (3n - 2) + (3n - 2) = 10n - 7 \approx 10n$
		
		\subsection{Perbandingan Kompleksitas}
		Kompleksitas pada ketiga algoritma adalah linear (berdasarkan flops). Pada ketiga algoritma ini dilakukan pengetesan terhadap matriks definit positif dengan ukuran $n = 10, 110, ... , 510$ sehingga didapat hasil sebagai berikut,\\
		\includegraphics[width=0.5\textwidth]{../compare_complexity.png}
		Terlihat bahwa semua kompleksitasnya linear, walaupun pada algoritma dengan \textit{partial pivoting} lebih tinggi. Hal ini mungkin disebabkan karena kompleksitas pada iterasi yang dilakukan atau pada proses swapping, walaupun kompleksitas flops adalah $10n$. Namun jika membandingkan \textit{partial pivoting} yang sudah dioptimisasi dengan yang belum, terlihat jelas perbedaan kompleksitasnya (dilakukan pada tridiagonal matriks definit positif dengan $n=1, 11, .., 101)$.
		\includegraphics[width=0.5\textwidth]{../compare_partial.png}
		
		\subsection{Analisis \textit{Error}}
		Algoritma yang telah diimplementasi harus dicek apakah solusi yang ditemukan memang benar pada SPL tridiagonal yang diberikan. \textit{Backward error} digunakan untuk memvalidasi solusi yang dihasilkan. Jika SPL $Ax = b$ didapatkan solusi $\hat{x}$ maka nilai dari \textit{backward error}-nya adalah $b - A\hat{x}$. Pada SPL, $b - A\hat{x}$ merupakan sebuah vektor, sehingga untuk menentukan nilai \textit{backward error} digunakan $||b - A\hat{x}||_{2}$ (panjang dari vektor solusi yang didapat). Solusi yang valid atau benar jika nilai \textit{backward error}-nya sama dengan atau mendekati 0.
		\\\\
		Pengujian pertama menggunakan algoritma $LU$ tridiagonal dan $LU$ tridiagonal menggunakan \textit{partial pivoting} terhadap 3 buah SPL tridiagonal yang masing - masing memiliki ukuran $3\times3, 5\times5$ dan $7\times7$ serta sebanyak 47 buah SPL tridiagonal acak dengan ukuran $n\times n$ ($4\le n \le 50$). Hasil pengujian didapatkan,
		\\
		\begin{center}
			\renewcommand{\arraystretch}{1.2} % Default value: 1
			\begin{tabular}{c|c}
				\textbf{Algoritma} & \textbf{\textit{Backward error}} \\
				\hline
				Sistem ($A$\textbackslash$b$) & 2.1296e-15\\
				$LU$ & 1.8043e-14\\
				$LU$ \textit{partial pivoting} & 1.5903e-14\\
			\end{tabular}
		\end{center}
		Pengujian kedua menggunakan semua algoritma terhadap 50 buah SPL tridiagonal definit positif dengan ukuran $n\times n$ ($1\le n \le 50$). Hasil pengujian didapatkan,\\
		\begin{center}
			\renewcommand{\arraystretch}{1.2} % Default value: 1
			\begin{tabular}{c|c}
				\textbf{Algoritma} & \textit{\textbf{Backward error}} \\
				\hline
				Sistem  ($A$\textbackslash$b$) & 9.6583e-15\\
				$LU$ & 6.8222e-15\\
				$LU$ \textit{partial pivoting} & 6.8222e-15\\
				\textit{Cholesky} & 1.2403e-14\\
			\end{tabular}
		\end{center}
		Ketiga algoritma yang diuji ternyata menghasilkan solusi yang valid karena nilai \textit{backward error} mendekati 0.
		\subsection{Tambahan}
		Berikut file - file kode sumber beserta kegunaannya,\\
		\textbf{backward\_substitution.m} : untuk $Ux = y$ secara umum\\
		\textbf{backward\_substitution\_trid.m} : untuk $Ux = y$ dari $A$ tridiagonal\\
		\textbf{factorization\_cholesky\_trid.m} : faktorisasi \textit{cholesky} pada $A$ tridiagonal\\
		\textbf{factorization\_lu\_partial\_trid.m} : faktorisasi $LU$ dengan \textit{partial pivoting}\\
		\textbf{factorization\_lu\_trid.m} : faktorisasi $LU$ tanpa \textit{partial pivoting}\\
		\textbf{forward\_elimination.m} : untuk $Ly = b$ secara umum\\
		\textbf{forward\_elimination\_trid.m} : untuk $Ly = b$ dari $A$ tridiagonal\\
		\textbf{generate\_postdef\_spl.m} : untuk menghasilkan spl definit positif\\
		\textbf{generate\_trid\_spl.m} : untuk menghasilkan spl tridiagonal acak\\
		\textbf{spl\_cholesky\_trid.m} : untuk menemukan solusi spl menggunakan \textit{cholesky}\\
		\textbf{spl\_lu\_partial\_trid.m} : untuk menemukan solusi spl menggunakan faktorisasi $LU$ tridiagonal yang optimal\\
		\textbf{spl\_lu\_partial\_unoptimize\_trid.m} : untuk menemukan solusi spl menggunakan faktorisasi $LU$ tridiagonal yang tidak optimal\\
		\textbf{spl\_lu\_trid.m} : untuk menemukan solusi spl menggunakan faktorisasi $LU$ tridiagonal tanpa pivoting\\
		\textbf{test\_compare\_partial\_complexity.m} : untuk membandingkan kompleksitas antara algoritma faktorisasi $LU$ partial pivoting optimal dengan yang tidak optimal \\
		\textbf{test\_complexity.m} : untuk membandingkan kompleksitas antar algoritma\\
		\textbf{test\_error\_spl.m} : untuk melihat \textit{backward error} dari setiap algoritma
		\section{Conclusion}
		Algoritma yang paling efisien adalah faktorisasi $LU$ tridiagonal (tanpa \textit{partial pivoting}). Algoritma yang menggunakan \textit{partial pivoting} merusak bentuk unik dari matriks tridiagonal, sehingga perlu dilakukan optimisasi (tentunya tidak akan sebaik algoritma  tanpa \textit{partial pivoting}). Optimisasi pada algoritma yang menggunakan \textit{partial pivoting} mengubah kompleksitas yang awalnya kuadratik menjadi linear. Kompleksitas algoritma yang menggunakan faktorisasi \textit{Cholesky} hampir mirip dengan algoritma tanpa \textit{pivoting}, mungkin karena nilai entri matriks pada proses tes sederhana. Hal yang menyebabkan kompleksitasnya meningkat jika angkanya tidak sederhana sehingga proses \textit{sqrt} tidak bisa dipandang menjadi satu flop lagi.
		\section{References}
		[1] \url{https://www.webpages.uidaho.edu/~barannyk/Teaching/LU_factorization_tridiagonal.pdf}
	\end{multicols}
	
\end{document}