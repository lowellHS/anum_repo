function x = spl_lup(A, b)
% find x that satisfy Ax = b, using lu factorization with partial pivoting
[L, U, P] = lup(A);
x = bs(U, fe(L, P*b));