function [L, U, P, Q] = lucp(A)
% Input matriks A
% output matriks L, U, P (row permutation) dan Q (column permutation)
[n,n]=size(A);
L=eye(n);                                   % L matrix
U=A;                                        % U matrix
P=zeros(n,n);                               % Row permutation matrix
Q=zeros(n,n);                               % Column permutation matrix    
p=1:n;                                      % row vector pivot
q=1:n;                                      % column vector pivot

for k=1:n-1
    % choose pivot
    [R, C] = find(abs(U(k:n,k:n)) == max(max(abs(U(k:n,k:n)))));
    r=R(1); c=C(1);r = r + k - 1;
    c = c + k - 1;
    
    %swapping row process
    tmpU=U(k,:); 
    tmpp=p(k);
    tmpl=L(k,1:k-1);
    tempU=U;
    
    U(k,:)=tempU(r,:); 
    p(k)=p(r);
    L(k,1:k-1)=L(r,1:k-1);
    
    U(r,:)=tmpU;
    p(r)=tmpp; 
    L(r,1:k-1)=tmpl;
    
    %swapping column process
    tmpU=U(:,k); 
    tmpq=q(k);
    tempU=U;
    
    U(:,k)=tempU(:,c); 
    q(k)=q(c);
    
    U(:,c)=tmpU;
    q(c)=tmpq;
    
    for l=k+1:n
        base = U(k,:);
        row = U(l,:);
        const = row(k)/U(k,k);
        row = row - const * U(k,:);
        U(l,:) = row;
        L(l,k) = const;
    end
end

for k=1:n
    P(k,p(k)) = 1;
    Q(q(k),k) = 1;
end