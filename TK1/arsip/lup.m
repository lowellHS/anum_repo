function [L, U, P] = lup(A)
%Input matriks A
%output matriks L & U dan vektor pivot p
[n,n]=size(A);
L=eye(n);                                   % L matrix
U=A;                                        % U matrix
P=zeros(n,n);                               % Permutation matrix
p=1:n;                                      % vector pivot

for k=1:n-1
    % choose pivot
    [c,m]=max(abs(U(k:n,k)));               % max in k-th col
    if c==0
        quit;                               % U is singular
    end
    
    m = m + k - 1;
    
    %swapping process
    tmpU=U(k,:); 
    tmpp=p(k);
    tmpl=L(k,1:k-1);
    tempU=U;
    
    U(k,:)=tempU(m,:); 
    p(k)=p(m);
    L(k,1:k-1)=L(m,1:k-1);
    
    U(m,:)=tmpU;
    p(m)=tmpp; 
    L(m,1:k-1)=tmpl;
    
    for l=k+1:n
        base = U(k,:);
        row = U(l,:);
        c = row(k)/U(k,k);
        row = row - c * U(k,:);
        U(l,:) = row;
        L(l,k) = c;
    end
end

for k=1:n
    P(k,p(k)) = 1;
end